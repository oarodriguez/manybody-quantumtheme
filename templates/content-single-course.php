<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header><h1 class="entry-title"><?php the_title(); ?></h1></header>
    <hr>
    <div class="row entry-content">
      <div class="course-overview col-md-12">
        <?php the_content(); ?>
      </div>
    </div>
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
