
<div class="footer content-info" role="contentinfo">
  <?php if (!is_front_page()): ?>
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
  <?php endif ?>

  <div class="container">
    <div class="row">
      <div class="col-sm-12 copyright-msg">
        <h4>&copy; 2014, Many Body Theories - <?php bloginfo('name'); ?>.</h4>
        <div class="cc">
          <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
          <br />
          <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Many Body Theories</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional License</a>.
        </div>
      </div>
    </div>
  </div>
</div>

<?php wp_footer(); ?>
