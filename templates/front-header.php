<h1 class="showcase-main-msg animated fadeInLeft">
  <div><?php bloginfo('name'); ?></div>
</h1>
<h2 class="showcase-sub-msg animated fadeInLeft">
  <?php bloginfo('description'); ?>
</h2>
