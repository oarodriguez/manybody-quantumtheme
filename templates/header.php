<div class="shell-bg">
  <div class="shell-fill"></div>
</div>

<!--[if lt IE 8]>
  <div class="browser-alert">
    <div class="container">
      <div class="row">
        <div class="alert alert-warning text-center browser-msg">
          <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
        </div>
      </div>
    </div>
  </div>
<![endif]-->


<header class="banner navbar navbar-inverse navbar-fixed-top thick" role="banner" id="main-navbar">
  <div class="navbar-pad">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
          <?php bloginfo('name'); ?>
        </a>
      </div>

      <nav class="collapse navbar-collapse" role="navigation">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-right'));
          endif;
        ?>
      </nav>
    </div>
  </div>
</header>

<section class="showcase">
  <div class="container showcase-content text-center">
    <?php if (is_front_page()): ?>
      <?php get_template_part('templates/front', 'header'); ?>
    <?php endif; ?>
  </div>
</section>
