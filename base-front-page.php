<?php get_template_part('templates/head'); ?>

<body <?php body_class(); ?>>

  <div class="shell animated">

    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>

    <?php include roots_template_path(); ?>

    <?php if (roots_display_sidebar()) : ?>
      <aside class="sidebar" role="complementary">
        <?php include roots_sidebar_path(); ?>
      </aside><!-- /.sidebar -->
    <?php endif; ?>

    <?php get_template_part('templates/footer'); ?>

  </div>

</body>
</html>
