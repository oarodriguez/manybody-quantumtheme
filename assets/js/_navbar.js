(function ($) {
  'use strict';

  var nav = $('#main-navbar');

  var scrollHandler = function () {
    if ($(window).scrollTop() > 200) {
      nav.removeClass('thick');
    } else {
      nav.addClass('thick');
    }
  };

  $(window).bind('scroll', scrollHandler);
  scrollHandler();

})(jQuery);
