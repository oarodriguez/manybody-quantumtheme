<?php get_template_part('templates/head'); ?>

<body <?php body_class('base-custom'); ?>>

  <div class="shell animated">
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>

    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main" role="main">
          <?php include roots_template_path(); ?>
        </main><!-- /.main -->

      </div><!-- /.content -->
    </div><!-- /.wrap -->

  </div>

  <?php get_template_part('templates/footer'); ?>

</body>
</html>
