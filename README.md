# [Many Body Theories "Quantum" Theme](https://bitbucket.org/oarodriguez/manybody-quantumtheme)
[![devDependency Status](https://david-dm.org/roots/roots/dev-status.svg)](https://david-dm.org/roots/roots#info=devDependencies)

Theme for [Many Body Theories website](http://manybody.fisica.unam.mx/), based on [Roots Theme](http://roots.io/).

* Source: [https://bitbucket.org/oarodriguez/manybody-quantumtheme](https://bitbucket.org/oarodriguez/manybody-quantumtheme)
* Homepage: [https://bitbucket.org/oarodriguez/manybody-quantumtheme](https://bitbucket.org/oarodriguez/manybody-quantumtheme)


## Installation and usage

As the theme is based on Roots the instructions to install it and use it are basically the same for Roots theme.

### Install Grunt and Bower

[Download and install node.js](http://nodejs.org/download/) before proceeding.

From the command line:

1. Install `grunt-cli` and `bower` globally with `npm install -g grunt-cli bower`.
2. Navigate to the theme directory, then run `npm install`. npm will look at `package.json` and automatically install the necessary dependencies. It will also automatically run `bower install`, which installs front-end packages defined in `bower.json`.
3. Run any of the various Grunt commands provided from the command line.

### Available Grunt commands

* `grunt dev` — Compile LESS to CSS, concatenate and validate JS
* `grunt watch` — Compile assets when file changes are made
* `grunt build` — Create minified assets that are used on non-development environments

