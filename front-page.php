<!-- Begin frontpage content -->

<section class="content animated">

  <!-- Begin About Us -->
  <section class="about-msg"><div class="container">

    <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', 'page'); ?>
    <?php endwhile; ?>

    <?php if (get_field('institutions')): ?>
    <div class="row">

    <?php
      while (has_sub_field('institutions')):
        $picture = get_sub_field('institution_picture');
        $url = $picture['url'];
    ?>

    <div class="col-sm-6 text-center">
      <img src="<?php echo $url ?>" alt="" class="img-responsive section-pic">
    </div>

    <?php
      endwhile;
    ?>

    </div>
    <?php endif ?>

    <?php the_field('frontpage_about_extra'); ?>

  </div>

  </section>
  <!-- End About Us -->


  <?php if (get_field('research_lines')): ?>
  <!-- Begin research lines -->
  <section class="research-lines">
    <div class="container">
      <h2 class="section-name">
        <?php _e('Research Lines', 'roots') ?>
      </h2>

      <div class="row">
      <?php
        while (has_sub_field('research_lines')):
          $name = get_sub_field('research_line_name');
          $picture = get_sub_field('research_line_picture');
          $picture_url = $picture['url'];
          $resource_link = get_sub_field('research_line_resource');
      ?>

        <div class="col-md-6 text-center">
          <div class="research-line">
            <a href="<?php echo $resource_link; ?>">
              <img src="<?php echo $picture_url;  ?>" alt="" class="img-responsive img-thumbnail">
            </a>
            <h3><a href="<?php echo $resource_link; ?>"><?php echo $name; ?></a></h3>
          </div>
        </div>

      <?php
        endwhile;
      ?>
      </div>

    </div>
  </section>
  <!-- End research lines -->

  <?php endif ?>


  <?php if (get_field('students_training')): ?>

  <!-- Begin students training -->
  <section class="students-training">
    <div class="container">
      <h2 class="section-name">
        <?php _e('Students Training', 'roots') ?>
      </h2>

      <?php the_field('students_training'); ?>

      <?php
      if (get_field('supporting_institutions')):
        while (has_sub_field('supporting_institutions')):
          $picture = get_sub_field('support_institution_picture');
          $url = $picture['url'];
      ?>

      <div class="col-sm-6 text-center">
        <img src="<?php echo $url;  ?>" alt="" class="img-responsive section-pic">
      </div>

      <?php
        endwhile;
      endif;
      ?>
    </div>
  </section>
    <!-- End Students training -->

  <?php endif ?>

</section>
<!-- End frontpage content -->
